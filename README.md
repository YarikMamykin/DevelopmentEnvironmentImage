# DevelopmentEnvironmentImage

## Description
This repository contains docker image spec for [DevelopmentEnvironment](https://codeberg.org/YarikMamykin/DevelopmentEnvironment)
Image, when built successfully, creates standalone container with custom `user` that contains all necessary packages for development environment. 
More details on package list can be found [here](https://codeberg.org/YarikMamykin/DevelopmentEnvironment)

### Build arguments
- DISTRO = name of base distribution (e.g. archlinux, ubuntu)
- ROOT_REPO = link to host, that contains [DevelopmentEnvironment](https://codeberg.org/YarikMamykin/DevelopmentEnvironment) packages and binaries
- GID = desired user group id
- UID = desired user id
- USERNAME = name of the user to create under image
- GIT_NAME = name for git user
- GIT_EMAIL = email for git user

### Build cmd
This image can be built within next command (e.g. for archlinux):
```
docker build \
	--build-arg DISTRO=archlinux \
	--build-arg ROOT_REPO="https://codeberg.org/YarikMamykin" \
	--build-arg UID=`id -u` \
	--build-arg GID=`id -g` \
	--build-arg USERNAME=user \
	--build-arg GIT_NAME=gitname \
	--build-arg GIT_EMAIL="somemail@domain.com" \
	-t arch-dev .
```

And for docker compose (for rootless docker replace devenv with devenv-rootless):
```
docker compose build devenv \
    --build-arg DISTRO=archlinux 
    --build-arg ROOT_REPO="https://codeberg.org/YarikMamykin" 
    --build-arg UID=`id -u` 
    --build-arg GID=`id -g` 
    --build-arg USERNAME=user 
	--build-arg GIT_NAME=gitname \
	--build-arg GIT_EMAIL="somemail@domain.com" 
```

### Important
In case of `docker compose` usage it is required to have `codebase` and `ssh-certs` docker volumes pre-created.
E.g. `docker volume create --name codebase --opt type=none --opt o=bind --opt device=<path_to_your_code_folder>`
