ARG DISTRO

FROM ${DISTRO}:latest

ARG DISTRO
ARG ROOT_REPO
ARG UID
ARG GID
ARG USERNAME
ARG GIT_NAME
ARG GIT_EMAIL

ENV DISTRO=${DISTRO}
ENV ROOT_REPO=${ROOT_REPO}
ENV GIT_NAME=${GIT_NAME}
ENV GIT_EMAIL=${GIT_EMAIL}

COPY helpers/${DISTRO}/help.sh /

COPY helpers/${DISTRO}/add_to_sudo.sh /

RUN chmod +x /help.sh && /help.sh && rm -frv /help.sh && \
		groupadd -g ${GID} ${USERNAME} && \
		useradd -u ${UID} -d /home/${USERNAME} -mg ${USERNAME} -s /bin/bash ${USERNAME} && \
		passwd -d root && \
		passwd -d ${USERNAME} && \
		chmod +x /add_to_sudo.sh && /add_to_sudo.sh && rm -frv /add_to_sudo.sh && \
		git clone ${ROOT_REPO}/DevelopmentEnvironment && \
    mv DevelopmentEnvironment/ /home/${USERNAME}/DevelopmentEnvironment && \
		chmod -R +x /home/${USERNAME}/DevelopmentEnvironment/ && \
		chown -R ${USERNAME}:${USERNAME} /home/${USERNAME}/DevelopmentEnvironment/

USER ${USERNAME}
WORKDIR /home/${USERNAME}/DevelopmentEnvironment

RUN ./install.sh

WORKDIR /home/${USERNAME}
RUN rm -frv /home/${USERNAME}/.ssh && ln -sf /mnt/ssh-certs .ssh && ln -sf /mnt/codebase codebase
